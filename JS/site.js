var productos = [
    { "ImageUrl": "images/timeline1.jpg", "Descripcion": "Café con galletas", "Precio": "$10.000" }, { "ImageUrl": "images/timeline6.jpg", "Descripcion": "Tea de Manzanilla", "Precio": "$2.500" }, { "ImageUrl": "images/timeline2.jpg", "Descripcion": "Cebolla", "Precio": "$3.000" }, { "ImageUrl": "images/timeline4.jpg", "Descripcion": "Frutos secos", "Precio": "$20.000" }, { "ImageUrl": "images/timeline3.jpg", "Descripcion": "Agua de panela", "Precio": "$5.000" }, { "ImageUrl": "images/timeline5.jpg", "Descripcion": "Espinaca", "Precio": "$4.500" }, { "ImageUrl": "images/timeline6.jpg", "Descripcion": "Tea de hierba buena", "Precio": "$7.000" }, { "ImageUrl": "images/timeline1.jpg", "Descripcion": "Late con galletas de avena", "Precio": "$12.000" }, { "ImageUrl": "images/timeline2.jpg", "Descripcion": "Hortalizas", "Precio": "$10.000" }, { "ImageUrl": "images/timeline4.jpg", "Descripcion": "Cafe quindio molido", "Precio": "$35.000" }, { "ImageUrl": "images/timeline3.jpg", "Descripcion": "Chocolate", "Precio": "$12.000" }, { "ImageUrl": "images/timeline6.jpg", "Descripcion": "Desayuno americano", "Precio": "$16.000" }
];

var blogIndex = [
    { "ImageUrl": "images/product5.jpg", "Titulo": "Nuevo gran sabor", "Fecha": "25 de octubre", "Entrada": "Prueba nuestro novedoso desayuno nutritivo y bajo en calorías no complejas, un delicioso café directamente extraído de las montañas del Quindío, acompañado de unas deliciosas galletas de avena o arroz." }, { "ImageUrl": "images/product3.jpg", "Titulo": "Vegetales Frescos", "Fecha": "25 de octubre", "Entrada": "Conoce nuestra cosecha de los mejores vegetales y legumbres, te ofrecemos diversidad de productos: Cebolla, cebollín, espinaca, lechuga, apio, espárragos y mucho más. No lo pienses dos veces y pide ahora." }, { "ImageUrl": "images/product1.jpg", "Titulo": "Lo mejor del café", "Fecha": "25 de octubre", "Entrada": "Conoce nuestra gran variedad de café, la diversidad en sabor y aroma de diferentes lugares de Colombia, desde café de Santander, hasta café del valle del cauca, pide ahora mismo, te sorprenderás." }
];

var recomendacionDia = { "Autor": "Alejo Cafetero", "Recomendacion": "Hoy nuestro experto en café, nos recomienda un delicioso café huilense acompañado de unas achiras. No te lo pierdas." };

function LoadAllProducts() {
    var productosHtml = '';
    var cantColumnas = 4;
    var contColumnas = 1;

    for (var i = 0; i < productos.length; i++) {

        if (contColumnas == 1) {
            productosHtml += '<article class="artproduct">'
        }

        productosHtml += '<div class="conimgproduct">';
        productosHtml += '<img src="' + productos[i].ImageUrl + '">';
        productosHtml += '<div class="textproduct">';
        productosHtml += '<p> ' + productos[i].Descripcion + ' </p>';
        productosHtml += '<span> ' + productos[i].Precio + ' </span>';
        productosHtml += '</div>';
        productosHtml += '</div>';

        contColumnas++;

        if (contColumnas > cantColumnas) {
            productosHtml += '</article>'
            contColumnas = 1;
        }
    }

    document.getElementById('section-productos').innerHTML = productosHtml;
}

function LoadInfoIndex() {
    var blogHtml = '';

    for (var i = 0; i < blogIndex.length; i++) {
        blogHtml += '<article class="artblog">';
        blogHtml += '<img src="' + blogIndex[i].ImageUrl + '" alt="Aqui va una imagen">';
        blogHtml += '<div class="textblog">';
        blogHtml += '<h3> ' + blogIndex[i].Titulo + ' </h3>';
        blogHtml += '<span> ' + blogIndex[i].Fecha + ' </span>';
        blogHtml += '<p> ' + blogIndex[i].Entrada + ' </p>';
        blogHtml += '<a href="blog.html"> Ver más </a>';
        blogHtml += '</div>';
        blogHtml += '</article>';
    }

    var recomendacionHtml = '<h2> RECOMENDACIONES DEL DÍA </h2>';
    recomendacionHtml += '<p> ' + recomendacionDia.Recomendacion + ' </p>';
    recomendacionHtml += '<p> ' + recomendacionDia.Autor + ' </p>';


    document.getElementById('blog-index').innerHTML = blogHtml;
    document.getElementById('recomendacion-dia').innerHTML = recomendacionHtml;
}

function ValidarContactenos() {
    if (document.getElementById('txtNombre').value.trim() == '') {
        alert('Por favor ingrese el nombre');
    }

    if (document.getElementById('txtEmail').value.trim() == '') {
        alert('Por favor ingrese el email');
    }

    if (document.getElementById('txtAsunto').value.trim() == '') {
        alert('Por favor ingrese el asunto');
    }

    if (document.getElementById('txtMensaje').value.trim() == '') {
        alert('Por favor ingrese el mensaje');
    }

    if (document.getElementById('txtEdad').value.trim() == '') {
        alert('Por favor ingrese la edad');
    }

    if (document.getElementById('txtCelular').value.trim() == '') {
        alert('Por favor ingrese la celular');
    }

    location.reload();
}